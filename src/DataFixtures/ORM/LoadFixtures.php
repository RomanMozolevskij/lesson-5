<?php
/**
 * Created by PhpStorm.
 * User: SauliusGTO3000
 * Date: 5/8/2018
 * Time: 11:45
 */

namespace App\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Nelmio\Alice\Loader\NativeLoader;

class LoadFixtures extends Fixture
{


    public function load(ObjectManager $manager)
    {
        $loader = new NativeLoader();
        $objectSet = $loader->loadFile(__DIR__.'/Fixtures.yaml')->getObjects();
        foreach ($objectSet as $object)
        {
            $manager->persist($object);
        }
        $manager->flush();
    }

}