<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 10/05/2018
 * Time: 18:15
 */

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="grades")
 */
class Grade
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Lecture", inversedBy="lectureGrades")
     */
    private $lecture;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="studentGrades")
     */
    private $student;

    /**
     * @ORM\Column(type="integer")
     */
    private $grade;


    //////////////////////////////////////////////////////////////////////////////////////////
    public function getLecture()
    {
        return $this->lecture;
    }


    public function setLecture($lecture): void
    {
        $this->lecture = $lecture;
    }


    public function getStudent()
    {
        return $this->student;
    }

    public function setStudent($student): void
    {
        $this->student = $student;
    }


    public function getGrade()
    {
        return $this->grade;
    }


    public function setGrade($grade)
    {
        $this->grade = $grade;
    }


    public function getId()
    {
        return $this->id;
    }


}