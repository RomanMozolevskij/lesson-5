<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 06/05/2018
 * Time: 13:28
 */

namespace App\Entity;


use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LectureRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="lectures")
 */
class Lecture
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $lectureName;


    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;


    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="lectures")
     * @ORM\JoinTable(name="lecture_user")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Article", mappedBy="lecture")
     */
    private $articles;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="teachersLectures")
     */
    private $teacher;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Grade", mappedBy="lecture")
     */
    private $lectureGrades;



///////////////////////////////////<-construct->//////////////////////////////////////////


    public function __construct()
    {
        $this->lectureGrades = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->articles = new ArrayCollection();
    }

////////////////////////////<-getters & setters->/////////////////////////////////////////

    /**
     * @return ArrayCollection|Grade[]
     */
    public function getLectureGrades()
    {
        return $this->lectureGrades;
    }

    /**
     * @param mixed $lectureGrades
     */
    public function setLectureGrades($lectureGrades): void
    {
        $this->lectureGrades = $lectureGrades;
    }

    /**
     * @return mixed
     */
    public function getLectureName()
    {
        return $this->lectureName;
    }

    /**
     * @param mixed $lectureName
     */
    public function setLectureName($lectureName): void
    {
        $this->lectureName = $lectureName;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @param mixed $articles
     */
    public function setArticles($articles): void
    {
        $this->articles = $articles;
    }

    /**
     * @return mixed
     */
    public function getTeacher() : ?User
    {
        return $this->teacher;
    }


    public function setTeacher(?User $teacher): self
    {
        $this->teacher = $teacher;

        return $this;
    }

    public function addUsers(User $user)
    {
        if($this->users->contains($user)){
            return;
        }

        $this->users[] = $user;
    }

    /**
     * @return ArrayCollection|User[]
     */
    public function getUsers()
    {
        return $this->users;
    }
/////////////////////////////<-custom functions->/////////////////////////////////////////

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function  updateTimestamps(): void
    {
        $dateTimeNow = new DateTime('now');
        $this->setUpdatedAt($dateTimeNow);
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt($dateTimeNow);
        }
    }

    public function __toString()
    {
        return $this->getLectureName();
    }


}