<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 02/05/2018
 * Time: 10:40
 */

namespace App\Entity;


use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="app_users")
 * @UniqueEntity(fields={"email"}, message="Email all ready exists!")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $firstName;


    /**
     * @ORM\Column(type="string")
     */
    private $lastName;

    /**
     * @ORM\Column(type="string")
     */
    private $password;

    private $plainPassword;

    /**
     *
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     *
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $roles = ['ROLE_STUDENT'];

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Lecture", mappedBy="users")
     */
    private $lectures;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Article", mappedBy="users")
     */
    private $articles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Lecture", mappedBy="teacher")
     */
    private $teachersLectures;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Article", mappedBy="teacher_article")
     */
    private $teachersArticles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Grade", mappedBy="student")
     */
    private $studentGrades;


    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    private $totalGrade;





/////////////////////////////////  <-construct->  /////////////////////////////////////////////////////

    public function __construct()
    {
        $this->teachersArticles = new ArrayCollection();
        $this->teachersLectures = new ArrayCollection();
        $this->lectures = new ArrayCollection();
        $this->articles = new ArrayCollection();
        $this->studentGrades = new ArrayCollection();

    }



////////////////////////////    <- getter & setters ->    /////////////////////////////////////////////


    public function getTotalGrade()
    {
        return $this->totalGrade;
    }


    public function setTotalGrade($totalGrade): void
    {
        $this->totalGrade = $totalGrade;
    }

    /**
     * @return ArrayCollection|Grade[]
     */
    public function getStudentGrades()
    {
        return $this->studentGrades;
    }


    public function setStudentGrades($studentGrades): void
    {
        $this->studentGrades = $studentGrades;
    }



    public function getLastName()
    {
        return $this->lastName;
    }


    public function setLastName($lastName): void
    {
        $this->lastName = $lastName;
    }

    public function getUsername()
    {
        return $this->email;
    }


    public function getEmail()
    {
        return $this->email;
    }


    public function setEmail($email): void
    {
        $this->email = $email;
    }


    public function getFirstName()
    {
        return $this->firstName;
    }


    public function setFirstName($firstName): void
    {
        $this->firstName = $firstName;
    }


    public function getPassword()
    {
        return $this->password;
    }


    public function setPassword($password): void
    {
        $this->password = $password;
    }


    public function getPlainPassword()
    {
        return $this->plainPassword;
    }


    public function setPlainPassword($password): void
    {
        $this->plainPassword = $password;
    }


    public function getCreatedAt()
    {
        return $this->createdAt;
    }


    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }


    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }


    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }



    public function setRoles($roles)
    {
       $this->roles = $roles;

    }

    public function getRoles()
    {
        $temRoles = $this->roles;

        if(!in_array('ROLE_STUDENT' || 'ROLE_TEACHER', $temRoles)){
            $temRoles[] = 'ROLE_STUDENT';
        }
        return $temRoles;
    }


    public function getId()
    {
        return $this->id;
    }


    public function getArticles()
    {
        return $this->articles;
    }


    public function setArticles($articles): void
    {
        $this->articles = $articles;
    }


    public function getLectures()
    {
        return $this->lectures;
    }

    /**
     * @return ArrayCollection|Lecture[]
     */
    public function getTeachersLectures()
    {
        return $this->teachersLectures;
    }


    /**
     * @return ArrayCollection|Article[]
     */
    public function getTeachersArticles()
    {
        return $this->teachersArticles;
    }

    public function getSalt()
    {
        return null;
    }

///////////////////////////////<-custom functions->/////////////////////////////////////////////
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function  updateTimestamps(): void
    {
        $dateTimeNow = new DateTime('now');
        $this->setUpdatedAt($dateTimeNow);
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt($dateTimeNow);
        }
    }


    public function eraseCredentials()
    {
        return null;
    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
        ));
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            ) = unserialize($serialized, ['allowed_classes' => false]);
    }


    public function getFullName()
    {
        return trim($this->getFirstName().' '.$this->getLastName());
    }


    public function __toString()
    {
        return $this->getFullName();
    }



}