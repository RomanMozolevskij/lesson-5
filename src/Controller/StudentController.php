<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 13/05/2018
 * Time: 11:35
 */

namespace App\Controller;


use App\Entity\Grade;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/student")
 */
class StudentController extends Controller
{

    /**
     * @Route("/", name="student_index", methods="GET")
     */
    public function index(): Response
    {

        $student = $this->getUser()->getId();


        $students = $this->getDoctrine()
            ->getRepository(Grade::class)
            ->findBy(['student' => $student]);


        return $this->render('student/index.html.twig', [
            'students' => $students,

        ]);
    }

}