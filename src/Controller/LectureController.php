<?php

namespace App\Controller;

use App\Entity\Grade;
use App\Entity\Lecture;
use App\Entity\User;
use App\Form\LectureType;
use App\Form\NewLectureType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/lecture")
 */
class LectureController extends Controller
{
    /**
     * @Route("/", name="lecture_index", methods="GET")
     */
    public function index(): Response
    {

        $user = $this->getUser()->getUsername();
        $teacherId = $this->getUser()->getId();
        $lectures = $this->getDoctrine()
            ->getRepository(Lecture::class)
            ->findBy(['teacher' => $teacherId]);

        return $this->render('lecture/index.html.twig', [
            'lectures' => $lectures,
            'user' => $user
        ]);
    }

    /**
     * @Route("/new", name="lecture_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {

        $lecture = new Lecture();
        $user = $this->getUser();
        $lecture->setTeacher($user);

        $form = $this->createForm(NewLectureType::class, $lecture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($lecture);
            $em->flush();

            return $this->redirectToRoute('lecture_index');
        }

        return $this->render('lecture/new.html.twig', [
            'lecture' => $lecture,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="lecture_show", methods="GET")
     */
    public function show(Lecture $lecture): Response
    {
        $user = $this->getUser()->getUsername();
        return $this->render('lecture/show.html.twig', [
            'lecture' => $lecture,
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/articles", name="lectures_articles", methods="GET")
     */
    public function showArticle(Lecture $lecture): Response
    {
        return $this->render('lecture/showArticles.html.twig', ['lecture' => $lecture]);
    }

    /**
     * @Route("/{id}/students", name="lectures_students", methods="GET")
     */
    public function showStudents(Lecture $lecture, Grade $grade): Response
    {

        return $this->render('lecture/showStudents.html.twig', [
            'lecture' => $lecture,
            'grade' => $grade,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="lecture_edit", methods="GET|POST")
     */
    public function edit(Request $request, Lecture $lecture): Response
    {
        $user = $this->getUser();
        $this->denyAccessUnlessGranted('edit', $user);
        $form = $this->createForm(LectureType::class, $lecture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('lecture_edit', ['id' => $lecture->getId()]);
        }

        return $this->render('lecture/edit.html.twig', [
            'lecture' => $lecture,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="lecture_delete", methods="DELETE")
     */
    public function delete(Request $request, Lecture $lecture, User $user): Response
    {
        $this->denyAccessUnlessGranted('delete', $user);
        if ($this->isCsrfTokenValid('delete'.$lecture->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($lecture);
            $em->flush();
        }

        return $this->redirectToRoute('lecture_index');
    }
}
