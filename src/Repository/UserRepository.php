<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 10/05/2018
 * Time: 17:22
 */

namespace App\Repository;


use App\Entity\User;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function createIsStudentQueryBuilder()
    {
        return $this->createQueryBuilder('user')
            ->andWhere('user.roles = :roles')
            ->setParameter('roles', 'ROLE_STUDENT');
    }


}