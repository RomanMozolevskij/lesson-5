<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 06/05/2018
 * Time: 15:21
 */

namespace App\Repository;




use App\Entity\User;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
use Symfony\Component\Security\Core\User\UserInterface;


class LectureRepository extends EntityRepository
{

    public function createLectureQueryBuilder()
    {

        return $this->createQueryBuilder('lecture')
            ->orderBy('lecture.lectureName', 'ASC');
    }


}