<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 24/04/2018
 * Time: 10:07
 */

namespace App\Doctrine;




use App\Entity\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class HashPasswordListener implements EventSubscriber
{


    private $passwordEncoder;

    public function __construct(UserPasswordEncoder $passwordEncoder)
    {

        $this->passwordEncoder = $passwordEncoder;
    }


    public function  prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if(!$entity instanceof User){
            return;
        }

        $encoded = $this->passwordEncoder->encodePassword($entity, $entity->getPlainPassword());
        $entity->setPassword($encoded);
    }

    public function  preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if(!$entity instanceof User){
            return;
        }

        $this->encodePassword($entity);
        $tulikas = $args->getEntityManager();
        $meta = $tulikas->getClassMetadata(get_class($entity));
        $tulikas->getUnitOfWork()->recomputeSingleEntityChangeSet($meta,$entity);
    }


    public function getSubscribedEvents()
    {
        return ['prePersist', 'preUpdate'];
    }


    public function encodePassword(User $entity)
    {
        $encoded = $this->passwordEncoder->encodePassword($entity, $entity->getPlainPassword());
        $entity->setPassword($encoded);
    }

}