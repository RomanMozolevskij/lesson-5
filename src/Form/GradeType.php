<?php

namespace App\Form;

use App\Entity\Grade;
use App\Entity\Lecture;
use App\Entity\User;
use App\Repository\LectureRepository;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GradeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('grade')
            ->add('lecture', EntityType::class, [
                'placeholder' => 'Lecture',
                'class' => Lecture::class,
                'query_builder' => function(LectureRepository $repo){
                    return $repo->createLectureQueryBuilder();
                }
            ])
            ->add('student', EntityType::class, [
                'placeholder' => 'Student',
                'class' => User::class,
                'choice_label' => 'fullName',
                'query_builder' => function(UserRepository $repo){
                    return $repo->createIsStudentQueryBuilder();
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Grade::class,
        ]);
    }
}
