<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180510101322 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE lecture_grade');
        $this->addSql('ALTER TABLE grades ADD lecture_id INT DEFAULT NULL, ADD student_id INT DEFAULT NULL, CHANGE grade grade VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE grades ADD CONSTRAINT FK_3AE3611035E32FCD FOREIGN KEY (lecture_id) REFERENCES lectures (id)');
        $this->addSql('ALTER TABLE grades ADD CONSTRAINT FK_3AE36110CB944F1A FOREIGN KEY (student_id) REFERENCES app_users (id)');
        $this->addSql('CREATE INDEX IDX_3AE3611035E32FCD ON grades (lecture_id)');
        $this->addSql('CREATE INDEX IDX_3AE36110CB944F1A ON grades (student_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE lecture_grade (grade_id INT NOT NULL, lecture_id INT NOT NULL, INDEX IDX_F2377BDAFE19A1A8 (grade_id), INDEX IDX_F2377BDA35E32FCD (lecture_id), PRIMARY KEY(grade_id, lecture_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE lecture_grade ADD CONSTRAINT FK_F2377BDA35E32FCD FOREIGN KEY (lecture_id) REFERENCES lectures (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE lecture_grade ADD CONSTRAINT FK_F2377BDAFE19A1A8 FOREIGN KEY (grade_id) REFERENCES grades (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE grades DROP FOREIGN KEY FK_3AE3611035E32FCD');
        $this->addSql('ALTER TABLE grades DROP FOREIGN KEY FK_3AE36110CB944F1A');
        $this->addSql('DROP INDEX IDX_3AE3611035E32FCD ON grades');
        $this->addSql('DROP INDEX IDX_3AE36110CB944F1A ON grades');
        $this->addSql('ALTER TABLE grades DROP lecture_id, DROP student_id, CHANGE grade grade INT NOT NULL');
    }
}
