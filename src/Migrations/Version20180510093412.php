<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180510093412 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE lecture_grade (grade_id INT NOT NULL, lecture_id INT NOT NULL, INDEX IDX_F2377BDAFE19A1A8 (grade_id), INDEX IDX_F2377BDA35E32FCD (lecture_id), PRIMARY KEY(grade_id, lecture_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE lecture_grade ADD CONSTRAINT FK_F2377BDAFE19A1A8 FOREIGN KEY (grade_id) REFERENCES grades (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE lecture_grade ADD CONSTRAINT FK_F2377BDA35E32FCD FOREIGN KEY (lecture_id) REFERENCES lectures (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE grades DROP lecture');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE lecture_grade');
        $this->addSql('ALTER TABLE grades ADD lecture VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
