<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180506150003 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE lectures ADD teacher_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE lectures ADD CONSTRAINT FK_63C861D041807E1D FOREIGN KEY (teacher_id) REFERENCES app_users (id)');
        $this->addSql('CREATE INDEX IDX_63C861D041807E1D ON lectures (teacher_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE lectures DROP FOREIGN KEY FK_63C861D041807E1D');
        $this->addSql('DROP INDEX IDX_63C861D041807E1D ON lectures');
        $this->addSql('ALTER TABLE lectures DROP teacher_id');
    }
}
